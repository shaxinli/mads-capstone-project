

# MADS 697/698 Capstone project

## Project Description

This is the project gitlab page for our MADS 697/698 Capstone project. We worked on datasets that are produced from the Adult Change in Thoughts (ACT) study. the objective of this project is to 
1. Identify protein expression signatures that are correlated with dementia
2. Build supervised and unsupervised ML models using the protein expression signatures
3. Perform feature selection/engineering of the RNAseq data and use these features to build supervised and unsupervised ML models to predict dementia
4. Build deep learning model using RNA expression data for dementia prediction


### Explantion of the original study and data
The Adult Changes in Thought (ACT) study is a population-based study of brain aging and incident dementia, conducted by Kaiser Permanente Washington Health Research Institute. A specific part of this study, the Aging, Dementia, and Traumatic Brain Injury Study, contains detailed neuropathologic (protein expression) and transcriptomic (RNA expression) characterization of brains from a unique cohort with demographic matched dementia and non-dementia patients. The original aim of the Aging, Dementia, and TBI Study was to study neuropathologic and molecular signatures as a result of severity of TBI. However, the availability of extensive protein and RNA data can potentially be used to identify molecular hallmarks of and to ultimately predict the likelihood of developing dementia. 

* http://aging.brain-map.org/overview/home
* https://help.brain-map.org/display/aging/Documentation

## Folder structure

**data**

Directory that stores all the data needed

**saved_model**

Directory to store selected nerual network models

**notebooks**

    - 20220415_TBI_protein_path_together.ipynb
    - 20220416_RNAseq-log-transformed-supervised-to-unsupervised.ipynb
    - 20220421 Deep learning-yangxin.ipynb

**dockerfile**

Docker file used to create docker containor 

**requirement.txt**

list of python libraries used, generated using pip freeze with python virtual vene

## How to run
Install docker daemon, please consult the office [docker documentation](https://docs.docker.com/get-docker/) for help. After the installation is completed, open you favourite termial and type 

docker run -9000:8888 shaxinli/capstone_project

open your broswer and type the following into the url bar

http://localhost:9000

## Credits
* Ying Li: Data downloading, interpretation, and cleaning; RNA seq data visualization and ML models
* Xin Miao: Protein and pathologic data analysis and ML models
* Yangxin Mou: RNA seq deep learning models, gitlab setup, docker container building and code integration

All members of the team contributed to writing the final report

